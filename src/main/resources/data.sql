INSERT INTO role (name)
SELECT * FROM (SELECT 'ADMIN') AS tmp
WHERE NOT EXISTS (
    SELECT name FROM role WHERE name = 'ADMIN'
) LIMIT 1;

INSERT INTO role (name)
SELECT * FROM (SELECT 'PROPIETARIO') AS tmp
WHERE NOT EXISTS (
    SELECT name FROM role WHERE name = 'PROPIETARIO'
) LIMIT 1;

INSERT INTO role (name)
SELECT * FROM (SELECT 'CLIENTE') AS tmp
WHERE NOT EXISTS (
    SELECT name FROM role WHERE name = 'CLIENTE'
) LIMIT 1;


insert into user (id, address, cellphone, email, lastname, password, phone, username) values (1, "a",123, "a@gmail.com", "b","$2a$10$iAuQHWws2UPXZmECKxXSi./kaglmmZYZRM5ZQ8LghYNgMFYvGJHY6", 1, "anne" );
insert into user_role (user_id, role_id) values (1, 1);

insert into user (id, address,  cellphone, email, lastname, password, phone, username) values (2, "pub",123, "pub@gmail.com", "b","$2a$10$iAuQHWws2UPXZmECKxXSi./kaglmmZYZRM5ZQ8LghYNgMFYvGJHY6", 1, "publico" );
insert into user_role (user_id, role_id) values (2, 3);

INSERT INTO city (id, name) VALUES (1,"la paz");
INSERT INTO city (id, name) VALUES (2,"cocha");