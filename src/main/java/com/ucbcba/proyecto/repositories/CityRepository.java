package com.ucbcba.proyecto.repositories;

import com.ucbcba.proyecto.entities.City;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface CityRepository extends CrudRepository<City,Integer> {
}
