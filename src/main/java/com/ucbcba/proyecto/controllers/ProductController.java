package com.ucbcba.proyecto.controllers;

import com.ucbcba.proyecto.entities.Product;
import com.ucbcba.proyecto.services.ProductService;
import com.ucbcba.proyecto.services.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ProductController {

    private RestaurantService restaurantService;

    @Autowired
    public void setRestaurantService(RestaurantService restaurantService) {
        this.restaurantService = restaurantService;
    }

    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model) {
        return "principal";
    }

    @RequestMapping(value = "/admin/product/new", method = RequestMethod.GET)
    public String newProduct(Model model) {
        model.addAttribute("product", new Product());
        model.addAttribute("restaurants",restaurantService.listAllRestaurants());
        return "productForm";
    }

    @RequestMapping(value = "/admin/product", method = RequestMethod.POST)
    public String save(Product product, Model model) {
        productService.saveProduct(product);
        return "redirect:/admin/products";
    }

    @RequestMapping(value = "/admin/product/{id}", method = RequestMethod.GET)
    public String showProduct(@PathVariable Integer id, Model model) {

        Product product = productService.getProductById(id);
        model.addAttribute("product", product);
        return "product";
    }

    @RequestMapping(value = "/admin/product/editar/{id}", method = RequestMethod.GET)
    public String editProduct(@PathVariable Integer id, Model model) {
        Product product = productService.getProductById(id);
        model.addAttribute("product", product);
        return "productForm";
    }

    @RequestMapping(value = "/admin/product/eliminar/{id}", method = RequestMethod.GET)
    public String deleteProduct(@PathVariable Integer id, Model model) {
        productService.deleteProduct(id);
        return "redirect:/admin/products";
    }

    @RequestMapping(value = "/admin/products", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("products", productService.listAllProduct());
        return "products";
    }

}
