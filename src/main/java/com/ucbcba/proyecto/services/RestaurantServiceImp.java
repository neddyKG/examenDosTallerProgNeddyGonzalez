package com.ucbcba.proyecto.services;

import com.ucbcba.proyecto.entities.Restaurant;
import com.ucbcba.proyecto.repositories.RestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class RestaurantServiceImp implements RestaurantService{


    private RestaurantRepository restaurantRepository;

    @Autowired
    @Qualifier(value = "restaurantRepository")
    public void setRestaurantRepository(RestaurantRepository restaurantRepository) {
        this.restaurantRepository = restaurantRepository;
    }
    @Override
    public Iterable<Restaurant> listAllRestaurants( ) {
       return restaurantRepository.findAll();
    }

    @Override
    public Restaurant getRestaurantById(Integer id) {
        return restaurantRepository.findOne(id);
    }

    @Override
    public Restaurant saveRestaurant(Restaurant restaurant) {
        return restaurantRepository.save(restaurant);
    }

    @Override
    public void deleteRestaurant(Integer id) {
        restaurantRepository.delete(id);
    }
}
