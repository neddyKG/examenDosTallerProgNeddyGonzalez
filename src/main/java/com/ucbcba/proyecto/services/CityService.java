package com.ucbcba.proyecto.services;

import com.ucbcba.proyecto.entities.City;

public interface CityService {

    Iterable<City> listAllCities();

    City getCityById(Integer id);

    City saveCity(City category);

    void deleteCity(Integer id);
}
